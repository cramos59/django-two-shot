from django import forms
from django.contrib.auth.forms import UserCreationForm


class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=150)
    password1 = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password2 = forms.CharField(
        max_length=150, widget=forms.PasswordInput, label="Confirm Password"

        )


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
