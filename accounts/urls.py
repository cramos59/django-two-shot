from django.urls import path
from .views import login_view, logout_view, signup
from receipts import views


urlpatterns = [
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('signup/', signup, name='signup'),
    path('receipts/category_list/', views.category_list, name='category_list'),
    path('receipts/account_list/', views.account_list, name='account_list'),
]
