from django.urls import path
from receipts.views import receipt_list
from .views import ReceiptListView, create_receipt
from . import views

urlpatterns = [
    path('receipt_list/', receipt_list, name='home'),
    path('', ReceiptListView.as_view(), name='home'),
    path('create/', create_receipt, name='create_receipt'),
    path('receipts/category_list/', views.category_list, name='category_list'),
    path('receipts/account_list/', views.account_list, name='account_list'),
]
