from .models import ExpenseCategory, Account, Receipt
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm
from django.urls import reverse
from django.shortcuts import render, redirect


# Create your views here.

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(purchaser=request.user)
    category_data = []

    for category in categories:
        receipts_count = Receipt.objects.filter(category=category).count()
        category_data.append({
            'name': category.name,
            'num_receipts': receipts_count
        })

    context = {
        'category_data': category_data
    }

    return render(request, 'receipts/category_list.html', context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(purchaser=request.user)
    account_data = []

    for account in accounts:
        receipts_count = Receipt.objects.filter(account=account).count()
        account_data.append({
            'name': account.name,
            'number': account.number,
            'num_receipts': receipts_count
        })

    context = {
        'account_data': account_data
    }

    return render(request, 'receipts/account_list.html', context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect(reverse('home'))

    else:
        form = ReceiptForm()

    context = {
        "form": form
    }

    return render(request, "receipts/create.html", context)


def receipt_list(request):
    receipts = Receipt.objects.all()
    context = {
        'receipts': receipts
        }
    return render(request, 'receipts/receipt_list.html', context)


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = 'receipts/receipt_list.html'
    context_object_name = 'receipts'

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)
